 /*

Author: Dinesh Ravilla

For Ozone Unit
MKRGSM1400
DHT Temperature Sensor
DFRobot Ozone Sensor

Sends an SMS alert when temperature and ozone are out of range
Turns off ozone units when ozone is out of range

*/

#include <Adafruit_Sensor.h>   
#include <dht.h>                      // libraries for temperature and humidity sensor
#include <dht_u.h>
#include <mkrgsm.h>                   // library for the processor
#include "DFR_ozone.h"                // library for ozone sensor

#define DELAY_MINUTES 1               // minutes of time between every reading

//MACROS for DHT11
#define DHTPIN 2                      // "Out" of temperature sensor is connected to digital pin 2
#define DHTTYPE DHT11                 // macro for DHT11

//variables for ozone sensor and ADDRESS_3 is for I2C communication
#define COLLECT_NUMBER 20
#define Ozone_IICAddress ADDRESS_3 

//Warning Limit for ozone; if you want to change, replace 1200 with desired value
//1200 = 0.5 ppm
#define WARNING_OZONE 1200

//warning limit for temperature in celcius
#define WARNING_TEMP 60

//IN pin of relay connects to digital PIN 5 of Arduino MKR GSM 1400
#define OZONE_RELAY 5


//initializations for temperature sensor, ozone, GSM and GPRS
DHT_Unified dht(DHTPIN, DHTTYPE);
DFR_ozone ozone;
GSM gsmAccess;
GSM_SMS sms;
GSMLocation location;
GPRS gprs;

uint32_t delayMS;  
float motor_temp;                      // motor_temp is the variable that stores temperature value from the sensor

// If you want to add or delete phone numbers for alerts, do it here
// PINNUMBER[], GPRS_APN[], GPRS_LOGIN[], GPRS_PASSWORD[] will be stored in a .h file
// SECRET_PINNUMBER, SECRET_GPRS_APN, SECRET_GPRS_LOGIN, SECRET_GPRS_PASSWORD => comes from the SIM card we are using.
const char PINNUMBER[] = SECRET_PINNUMBER;     
const char textNum1[20]    = "5138852599";
const char textNum2[20]    = "8434786314";
const char textNum3[20]    = "+5511961971577";
const char textNum4[20]    = ""; 
const char textNum5[20]    = "";
const char GPRS_APN[]      = SECRET_GPRS_APN;
const char GPRS_LOGIN[]    = SECRET_GPRS_LOGIN;
const char GPRS_PASSWORD[] = SECRET_GPRS_PASSWORD;
char senderNumber[20];

//variables to make sure we establish the GSM & GPRS connection
// false = not connected
bool gsm_connected = false;
bool gprs_connected = false;

//ozone_flag variable is used to turn off the ozone units if the ozone warning limit is reached
int ozone_flag = 0;


//setup() == initializations at the start up
void setup() {
  
  Serial.begin(9600);
  gprs.setTimeout(15000);        //if GPRS connection is not established in 15 seconds, moves on
  gsmAccess.setTimeout(15000);   // GSM time out = 15 seconds
  
  //start GSM
  //tries to establish GSM connection
  //if the connection is not established even after 3 times, moves on to the next lines of code
  int gsm_try = 0;
  while(!gsm_connected && gsm_try < 3) {
    if(gsmAccess.begin() == GSM_READY) {
      gsm_connected = true;
      break;
    } else {
      Serial.println("GSM Not connected");
      delay(1000);
    }
    delay(2000);
    gsm_try++;
  }
  
  //lets you know whether the GSM connection is established or not
  //"Serial.println("..."); is not actually needed in the code, but it's useful while debugging
  if(gsm_connected) {
    Serial.println("GSM Connected");
  } else {
    Serial.println("GSM Not Connected");
  }
  
  //below code upto line 131 is used to establish GPRS connection, just like GSM
  //commented the code, because we weren't tracking these units, uncommenting the below code will establish GPRS connection.
  /*
  //start GPRS
  int gprs_try = 0;
  while(!gprs_connected && gprs_try < 3) {
    if((gsmAccess.begin(PINNUMBER) == GSM_READY) && 
       (gprs.attachGPRS(GPRS_APN, GPRS_LOGIN, GPRS_PASSWORD) == GPRS_READY)) {
         gprs_connected = true;
         break;
    } else {
      Serial.println("GPRS Not Connected");
      delay(1000);
    }
    delay(2000);
    gprs_try++;
  }
  
  if(gprs_connected) {
    Serial.println("GPRS Connected");
  }
  
  location.begin();
  */
  
  
  //dht - temperature and humidity sensor
  dht.begin();
  sensor_t sensor;
  dht.temperature().getSensor(&sensor);
  delayMS = sensor.min_delay / 1000;
  
  
  //Ozone - Arduino establishes connection with Ozone sensor
  //Serial.println("...") is not necessary in the actual code, but it's useful while debugging
  int ozone_try = 0;
  while(!Ozone.begin(Ozone_IICAddress) && ozone_try < 5) {
    Serial.println("I2C Device Number Error!");
    delay(2000);
    ozone_try++;
  }
  Serial.println("I2C connect success!");
  
  Ozone.SetModes(MEASURE_MODE_PASSIVE);  
}


void loop() {
  
  delay(DELAY_MINUTES*30*delayMS);       //delay ~5 seconds of time for the processor to setup 
  gprs.setTimeout(10000);                // timout for GSM & GPRS connections
  gsmAccess.setTimeout(10000);
  
  //if GSM connection is not established in the setup(), tries to establish the connection here
  if(gsm_connected == false) {  
    int gsm_try1 = 0;
    while(!gsm_connected && gsm_try1 < 2) {           
      if(gsmAccess.begin() == GSM_READY) {
        gsm_connected = true;
        break;
      } else {
        Serial.println("GSM Not Connected");
        delay(1000);
      }
      delay(2000);
      gsm_try1++;
    } 
  }
    
  int temp_flag = 0;            //interrupt for temperature sensor
  int sms_flag = 0;             //interrupt for received SMS
  
  //to get the reading for temperature - upto line 188
  sensors_event_t event;
  dht.temperature().getEvent(&event);
  if(isnan(event.temperature)) {
    Serial.println(F("Error reading temperature!"));
  } 
  else {
    motor_temp = event.temperature;
  }
  
  //ozone reading
  int16_t ozoneConcentration = Ozone.ReadOzoneData(COLLECT_NUMBER);
  
  
  /*
   * this while loop runs infinitely and monitors temperature and ozone
   * if the temperature or ozone is out of the warning limits temp_flag & ozone_flag will be set
   * and comes out of the while loop to send the text message to the customer
   * customer can also send a message to the unit. This loop monitors the received text messages.
   * if there is a text message, sms_flag will be set; control comes out of the loop and executes appropriately
   */
  while(1) {
  
    if(isnan(event.temperature)) {
      Serial.println(F("Error reading temperature!"));
    } 
    else {
      motor_temp = event.temperature;
    }
    
    ozoneConcentration = Ozone.ReadOzoneData(COLLECT_NUMBER);
    
    
    // if temperature is higher than warning temperature, sets the flag temp_flag and goes out of the while loop
    // to send the text message to the customer
    // we are not setting off the relay if the temperature is high, we are just sending a text message to the customer
    if(motor_temp > WARNING_TEMP) {   
      //Serial.print("TEMP:");
      //Serial.println(motor_temp);
      temp_flag = 1;  
      break;
    }
    
    // if ozone is higher than warning, sets ozone_flag; sets off the relay and waits for 30 seconds; goes out of the while loop 
    // to send the text message to the customer
    if(ozoneConcentration > WARNING_OZONE) {
      //Serial.print("OZONE:");
      //Serial.println(ozoneConcentration);
      ozone_flag = 1;
      digitalWrite(OZONE_RELAY, HIGH);
      delay(30000);
      break;
    }
    
    //below 2 lines are not neccessary for the program; useful for debugging
    Serial.println(motor_temp);
    Serial.println(ozoneConcentration);
    
    delay(1000);
    
    //if the unit received an alarm, sets sms_flag and goes out of the while loop
    if(sms.available()) {
      sms_flag = 1;
      break;
    } 
  } //end of while loop
  
  //sends text message to the customer numbers - upto line 311
  // textNum1 ... textNum5 are the phone numbers - at the top of the program
  if(temp_flag == 1 && ozone_flag == 1) {
    sms.beginSMS(textNum1);
    sms.print("Brazil NBOT_1: Temperature and ozone are high. NBOT is OFF");
    sms.endSMS();
    
    sms.beginSMS(textNum2);
    sms.print("Brazil NBOT_1: Temperature and ozone are high. NBOT is OFF");
    sms.endSMS();
    
    sms.beginSMS(textNum3);
    sms.print("Brazil NBOT_1: Temperature and ozone are high. NBOT is OFF");
    sms.endSMS();
    
    sms.beginSMS(textNum4);
    sms.print("Brazil NBOT_1: Temperature and ozone are high. NBOT is OFF");
    sms.endSMS();
    
    sms.beginSMS(textNum5);
    sms.print("Brazil NBOT_1: Temperature and ozone are high. NBOT is OFF");
    sms.endSMS();
    
    digitalWrite(OZONE_RELAY, LOW);
    ozone_flag = 0;                  // reset the flags for next encounter
    temp_flag = 0;   
    
  } else if(ozone_flag == 1) {
    sms.beginSMS(textNum1);
    sms.print("Brazil NBOT_1: Ozone level increasing. NBOT is OFF");
    sms.endSMS();
  
    sms.beginSMS(textNum2);
    sms.print("Brazil NBOT_1: Ozone level increasing. NBOT is OFF");
    sms.endSMS();
    
    sms.beginSMS(textNum3);
    sms.print("Brazil NBOT_1: Ozone level increasing. NBOT is OFF");
    sms.endSMS();
  
    sms.beginSMS(textNum4);
    sms.print("Brazil NBOT_1: Ozone level increasing. NBOT is OFF");
    sms.endSMS();
    
    sms.beginSMS(textNum5);
    sms.print("Brazil NBOT_1: Ozone level increasing. NBOT is OFF");
    sms.endSMS();
  
    digitalWrite(OZONE_RELAY, LOW);
    ozone_flag = 0;
    
  } else if(temp_flag == 1) {
    sms.beginSMS(textNum1);
    sms.print("Brazil NBOT_1: Temperature is high. Turn OFF the NBOT for few minutes");
    sms.endSMS();
    
    sms.beginSMS(textNum2);
    sms.print("Brazil NBOT_1: Temperature is high. Turn OFF the NBOT for few minutes");
    sms.endSMS();
    
    sms.beginSMS(textNum3);
    sms.print("Brazil NBOT_1: Temperature is high. Turn OFF the NBOT for few minutes");
    sms.endSMS();
    
    sms.beginSMS(textNum4);
    sms.print("Brazil NBOT_1: Temperature is high. Turn OFF the NBOT for few minutes");
    sms.endSMS();
    
    sms.beginSMS(textNum5);
    sms.print("Brazil NBOT_1: Temperature is high. Turn OFF the NBOT for few minutes");
    sms.endSMS();
    
    temp_flag = 0;
  }  
  
  //this is to gather temperature and ozone data at that instant
  String serial_string = "Brazil NBOT_1    ";
  String temp_string = "Temperature:";
  temp_string.concat(motor_temp);
  String ozone_string = "  Ozone:";
  ozone_string.concat(ozoneConcentration);
  String text_str = serial_string + temp_string + ozone_string;
  
  
  //if an SMS is received in the while loop above and if the first letter of the 
  // text is 'G', the unit sends out a text message with data from line 319 == "unit name_temperature & ozone readings"
  if(sms_flag) {
    Serial.println("Message received from: ");
    sms.remoteNumber(senderNumber, 20);
    Serial.println(senderNumber);
    
    if(sms.peek() == 'G') {
      sms.flush();
      String text_loc = "O3 Unit:  https://www.google.com/maps/place/" + lat + "," + lon + "   " + "Accuracy: +/- " + acc + "m";
      delay(1000);
      sms.beginSMS(senderNumber);
      sms.print(text_str);
      sms.endSMS(); 
    }
    
    //deletes thes previous message sent to the unit from customer
    sms.flush();
    Serial.println("message deleted");
    sms_flag = 0;        // reset the sms_flag
  }
  
}
