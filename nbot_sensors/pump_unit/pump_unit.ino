
 /*

Author: Dinesh Ravilla

For Pump Unit
MKRGSM1400
DHT Temperature Sensor
DFRobot Ozone Sensor
Pressure transducer

Sends an SMS alert when temperature, pressure and ozone are out of range
Turns off ozone units when ozone is out of range
Turns off VFD when pressure is out of range

*/

//S.No: Brazil NBOT_3

// All the annotations are exactly same as the ozone_unit program; changes will be related to only the pressure transducer and PRESSURE_RELAY
 
#include <Adafruit_Sensor.h>   //libraries for DHT11
#include <dht.h>
#include <dht_u.h>
#include <mkrgsm.h>       //library for MKR GSM 1400
#include "DFR_ozone.h"   //library for Ozone Sensor

#define DELAY_MINUTES 1    

#define DHTPIN 2    // Digital Pin connected to the DHT Sensor
#define DHTTYPE DHT11

#define COLLECT_NUMBER 20
#define Ozone_IICAddress ADDRESS_3

//Warning Limits for temperature, pressure, ozone
//Warning Limit for ozone; if you want to change, replace 1200 with desired value
//1200 = 0.5 ppm
#define WARNING_OZONE 1200
#define WARNING_TEMP 60

//warning limit for pressure (depends on the pressure transducer output type)
//below levels are voltage 0.5V - 4.5V
#define WARNING_MAX_PSI 4.5
#define WARNING_MIN_PSI 0.5

//PINS
//PRESSURE_RELAY connects to the pin number 3. 
#define PRESSURE_RELAY 3

//initializations
DHT_Unified dht(DHTPIN, DHTTYPE);
GSM gsmAccess;
GSM_SMS sms;
GSMLocation location;
GPRS gprs;
DFR_ozone Ozone;

uint32_t delayMS;
float motor_temp;

String text;

//MACRO for MKRGSM 1400
const char PINNUMBER[]     = SECRET_PINNUMBER;
const char textNum1[20]    = "5138852599";
const char textNum2[20]    = "8434786314";
const char textNum3[20]    = "8434426275";
const char textNum4[20]    = ""; 
const char textNum5[20]    = "";
const char GPRS_APN[]      = SECRET_GPRS_APN;
const char GPRS_LOGIN[]    = SECRET_GPRS_LOGIN;
const char GPRS_PASSWORD[] = SECRET_GPRS_PASSWORD;

char senderNumber[20];

bool gsm_connected = false;
bool gprs_connected = false;
bool ozone_connected = false;

//setup() == initializations at the start up
void setup() {

  Serial.begin(9600);
 
  delay(1000);
  pinMode(PRESSURE_RELAY, OUTPUT);      //makes the PRESSURE_RELAY pin as output
  
  gprs.setTimeout(15000);
  gsmAccess.setTimeout(15000);

  //start GSM
  //tries to establish GSM connection
  //if the connection is not established even after 3 times, moves on to the next lines of code
  int gsm_try = 0;
  while (!gsm_connected && gsm_try < 3) {
    //Serial.print("trying");
    //Serial.println("in");
    if (gsmAccess.begin() == GSM_READY) { 
      delay(1000);
      gsm_connected = true;
      break;
    } else {
      Serial.println("GSM Not Connected");
      delay(1000);
    }
    delay(2000);
    gsm_try++;
  }
  
  //lets you know whether the GSM connection is established or not
  //"Serial.println("..."); is not actually needed in the code, but it's useful while debugging
  if (gsm_connected) {
    Serial.println("GSM Connected");
  } else {
    Serial.println("GSM Not Connected");
  }
  
  //below code upto line 131 is used to establish GPRS connection, just like GSM
  //commented the code, because we weren't tracking these units, uncommenting the below code will establish GPRS connection.
  /*
  //start GPRS
  int gprs_try = 0;
  while (!gprs_connected && gprs_try < 2) {
    if ((gsmAccess.begin(PINNUMBER) == GSM_READY) &&
        (gprs.attachGPRS(GPRS_APN, GPRS_LOGIN, GPRS_PASSWORD) == GPRS_READY)) {
      gprs_connected = true;
      break;
    } else {
      Serial.println("GPRS Not Connected");
      delay(1000);
    }
    delay(2000);
    gprs_try++;
  }

  if (gprs_connected) {
    Serial.println("GPRS Connected");
  }
  location.begin();
  */

  //dht - temperature and humidity sensor
  dht.begin();
  sensor_t sensor;
  dht.temperature().getSensor(&sensor);
  delayMS = sensor.min_delay / 1000;
  
  //Ozone - Arduino establishes connection with Ozone sensor
  //Serial.println("...") is not necessary in the actual code, but it's useful while debugging
  int ozone_try = 0;
  while (!Ozone.begin(Ozone_IICAddress) && ozone_try < 5) {
    Serial.println("I2C Device Number Error!");
    delay(2000);
    ozone_try++;
  }

  Serial.println("I2C connect success!");

  Ozone.SetModes(MEASURE_MODE_PASSIVE);
}


void loop() {
  
  delay(DELAY_MINUTES * 30 * delayMS);   //DELAY_MINUTES between every reading
  gprs.setTimeout(10000);
  gsmAccess.setTimeout(10000);
  
  //if GSM connection is not established in the setup(), tries to establish the connection here
  if (gsm_connected == false) {
    int gsm_try1 = 0;
    while (!gsm_connected && gsm_try1 < 2) {
      if (gsmAccess.begin() == GSM_READY) {
        gsm_connected = true;
        break;
      } else {
        Serial.println("GSM Not Connected");
        delay(1000);
      } 
      delay(2000);
      gsm_try1++;
    }
  }

  //int c;
  
  //interrupts for temperature, max pressure, min pressure, ozone and sms
  int temp_flag = 0;
  int ozone_flag = 0;
  int psi_max_flag = 0;
  int psi_min_flag = 0;
  int pressure_flag = 0;
  int sms_flag = 0;
  
  //temperature reading
  sensors_event_t event;
  dht.temperature().getEvent(&event);
  if (isnan(event.temperature)) {
    Serial.println(F("Error reading temperature!"));
  }
  else {
    motor_temp = event.temperature;
  }
  
  //ozone reading
  int16_t ozoneConcentration = Ozone.ReadOzoneData(COLLECT_NUMBER);
  
  //ADC for pressure transducer
  int sensorVal = analogRead(A1);
  float psi_voltage = (sensorVal * (4.5 / 1023.0));
  
  
  //for debugging
  Serial.print("Temperature:"); Serial.println(motor_temp);
  Serial.print("Ozone:"); Serial.println(ozoneConcentration);
  Serial.print("Psi:"); Serial.println(psi_voltage);
  
  //digitalWrite(PRESSURE_RELAY, LOW);
  delay(5000);
  
  //since we don't want to trigger the minimum pressure flag while the machine is starting up, this loop will execute until the
  //pressure reaches a certian operating value ~ 25-30 psi
  while(psi_voltage < 2.5) {
    sensorVal = analogRead(A1);
    Serial.print("Analog:");
    Serial.println(sensorVal);
    psi_voltage = (sensorVal * (4.5/1023.0)) - 1.0;
    Serial.print("Digital:");
    Serial.println(psi_voltage);
    delay(1000);
  }
  
  delay(3000);
  
  
  /*
   * this while loop runs infinitely and monitors temperature and ozone
   * if the temperature or ozone is out of the warning limits temp_flag & ozone_flag will be set
   * and comes out of the while loop to send the text message to the customer
   * customer can also send a message to the unit. This loop monitors the received text messages.
   * if there is a text message, sms_flag will be set; control comes out of the loop and executes appropriately
   */
  
  while(1) {
    
    if (isnan(event.temperature)) {
      Serial.println(F("Error reading temperature!"));
    }
    else {
      motor_temp = event.temperature;
    }

    int16_t ozoneConcentration = Ozone.ReadOzoneData(COLLECT_NUMBER);
    
    sensorVal = analogRead(A1);
    Serial.println("analog:");
    Serial.println(sensorVal);
    psi_voltage = (sensorVal * (4.5/1023.0)) - 1.0;
    
    //if temperature is greater than warning level, sets the temp_flag
    if (motor_temp > WARNING_TEMP) {
      //Serial.print("TEMP:");
      //Serial.println(motor_temp);
      temp_flag = 1;
    }
    
    //if ozone is greater than warning level, sets the ozone_flag
    if (ozoneConcentration > WARNING_OZONE) {
      ozone_flag = 1;
    }
    
    //if pressure is less than minimum warning level of pressure, sets the psi_min_flag
    if (psi_voltage < WARNING_MIN_PSI) {
      psi_min_flag = 1;
      delay(3000);
    }
    
    //if pressure is greater than maximum warning level of pressure, sets the psi_max_flag
    if (psi_voltage > WARNING_MAX_PSI) {
      psi_max_flag = 1; 
      delay(3000);
    }
    
    if(psi_min_flag || psi_max_flag) {
      delay(30000);
      sensorVal = analogRead(A1);
      psi_voltage = (sensorVal * (4.5/1023.0)) - 1.0;
      
      if(psi_voltage > WARNING_MAX_PSI) {
        psi_max_flag = 1;
      } else {
        psi_max_flag = 0;
      }
      
      if(psi_voltage < WARNING_MIN_PSI) {
        psi_min_flag = 1;
      } else {
        psi_min_flag = 0;
      }
    }
    
    if(psi_min_flag || psi_max_flag) {
      delay(30000);
      sensorVal = analogRead(A1);
      psi_voltage = (sensorVal * (4.5/1023.0)) - 1.0;
      if(psi_voltage > WARNING_MAX_PSI) {
        psi_max_flag = 1;
      } else {
        psi_max_flag = 0;
      }
      
      if(psi_voltage < WARNING_MIN_PSI) {
        psi_min_flag = 1;
      } else {
        psi_min_flag = 0;
      }
    }
    
    if(psi_min_flag || psi_max_flag) {
      delay(30000);
      sensorVal = analogRead(A1);
      psi_voltage = (sensorVal * (4.5/1023.0)) - 1.0;
      if(psi_voltage > WARNING_MAX_PSI) {
        psi_max_flag = 1;
      } else {
        psi_max_flag = 0;
      }
      
      if(psi_voltage < WARNING_MIN_PSI) {
        psi_min_flag = 1;
      } else {
        psi_min_flag = 0;
      }
    }
    
    if(psi_min_flag || psi_max_flag) {
      delay(30000);
      sensorVal = analogRead(A1);
      psi_voltage = (sensorVal * (4.5/1023.0)) - 1.0;
      if(psi_voltage > WARNING_MAX_PSI) {
        psi_max_flag = 1;
      } else {
        psi_max_flag = 0;
      }
      
      if(psi_voltage < WARNING_MIN_PSI) {
        psi_min_flag = 1;
      } else {
        psi_min_flag = 0;
      }
    }
    

    if (ozone_flag || psi_min_flag || psi_max_flag) {
      //Serial.print("OZONE:");
      //Serial.println(ozoneConcentration);
      //ozone_flag = 1;
      
      digitalWrite(PRESSURE_RELAY, HIGH);
      delay(30000);
      digitalWrite(PRESSURE_RELAY, LOW);
      break;
    }
    
    delay(1000);
    
    
    
    if (sms.available()) {
      sms_flag = 1;
      break;
    }
    
    Serial.print("Temperature:"); Serial.println(motor_temp);
    Serial.print("Ozone:"); Serial.println(ozoneConcentration);
    Serial.print("Psi:"); Serial.println(psi_voltage);

  }
  
  if (temp_flag == 1 && ozone_flag == 1 && psi_max_flag == 1) {
    sms.beginSMS(textNum1);
    sms.print(" Brazil NBOT_3: Temperature, pressure and ozone are high. NBOT is OFF");
    sms.endSMS();

    sms.beginSMS(textNum2);
    sms.print(" Brazil NBOT_3: Temperature, pressure and ozone are high. NBOT is OFF");
    sms.endSMS();
    
    sms.beginSMS(textNum3);
    sms.print(" Brazil NBOT_3: Temperature, pressure and ozone are high. NBOT is OFF");
    sms.endSMS();

    sms.beginSMS(textNum4);
    sms.print(" Brazil NBOT_3: Temperature, pressure and ozone are high. NBOT is OFF");
    sms.endSMS();
    
    sms.beginSMS(textNum5);
    sms.print(" Brazil NBOT_3: Temperature, pressure and ozone are high. NBOT is OFF");
    sms.endSMS();
    
    ozone_flag = 0;
    temp_flag = 0;
    psi_max_flag = 0;

  } else if (temp_flag == 1 && ozone_flag == 1) {
    sms.beginSMS(textNum1);
    sms.print(" Brazil NBOT_3: Temperature and ozone are high. NBOT is OFF");
    sms.endSMS();

    sms.beginSMS(textNum2);
    sms.print(" Brazil NBOT_3: Temperature and ozone are high. NBOT is OFF");
    sms.endSMS();
    
    sms.beginSMS(textNum3);
    sms.print(" Brazil NBOT_3: Temperature and ozone are high. NBOT is OFF");
    sms.endSMS();

    sms.beginSMS(textNum4);
    sms.print(" Brazil NBOT_3: Temperature and ozone are high. NBOT is OFF");
    sms.endSMS();
    
    sms.beginSMS(textNum5);
    sms.print(" Brazil NBOT_3: Temperature and ozone are high. NBOT is OFF");
    sms.endSMS();

    temp_flag = 0;
    ozone_flag = 0;

  } else if (temp_flag == 1 && psi_max_flag == 1) {
    sms.beginSMS(textNum1);
    sms.print(" Brazil NBOT_3: Temperature and pressure are high. NBOT is OFF");
    sms.endSMS();

    sms.beginSMS(textNum2);
    sms.print(" Brazil NBOT_3: Temperature and pressure are high. NBOT is OFF");
    sms.endSMS();
    
    sms.beginSMS(textNum3);
    sms.print(" Brazil NBOT_3: Temperature and pressure are high. NBOT is OFF");
    sms.endSMS();

    sms.beginSMS(textNum4);
    sms.print(" Brazil NBOT_3: Temperature and pressure are high. NBOT is OFF");
    sms.endSMS();
    
    sms.beginSMS(textNum5);
    sms.print(" Brazil NBOT_3: Temperature and pressure are high. NBOT is OFF");
    sms.endSMS();

    temp_flag = 0;
    psi_max_flag = 0;

  } else if (ozone_flag == 1 && psi_max_flag == 1) {
    sms.beginSMS(textNum1);
    sms.print(" Brazil NBOT_3: Ozone and Pressure are high. NBOT is OFF");
    sms.endSMS();

    sms.beginSMS(textNum2);
    sms.print(" Brazil NBOT_3: Ozone and Pressure are high. NBOT is OFF");
    sms.endSMS();
    
    
    sms.beginSMS(textNum3);
    sms.print(" Brazil NBOT_3: Ozone and Pressure are high. NBOT is OFF");
    sms.endSMS();

    sms.beginSMS(textNum4);
    sms.print(" Brazil NBOT_3: Ozone and Pressure are high. NBOT is OFF");
    sms.endSMS();
    
    sms.beginSMS(textNum5);
    sms.print(" Brazil NBOT_3: Ozone and Pressure are high. NBOT is OFF");
    sms.endSMS();


    ozone_flag = 0;
    psi_max_flag = 0;

  } else if (ozone_flag == 1) {
    sms.beginSMS(textNum1);
    sms.print(" Brazil NBOT_3: Ozone level increasing. NBOT is OFF");
    sms.endSMS();

    sms.beginSMS(textNum2);
    sms.print(" Brazil NBOT_3: Ozone level increasing. NBOT is OFF");
    sms.endSMS();
    
    sms.beginSMS(textNum3);
    sms.print(" Brazil NBOT_3: Ozone level increasing. NBOT is OFF");
    sms.endSMS();

    sms.beginSMS(textNum4);
    sms.print(" Brazil NBOT_3: Ozone level increasing. NBOT is OFF");
    sms.endSMS();
    
    sms.beginSMS(textNum5);
    sms.print(" Brazil NBOT_3: Ozone level increasing. NBOT is OFF");
    sms.endSMS();

    ozone_flag = 0;

  } else if (psi_max_flag == 1) {
    sms.beginSMS(textNum1);
    sms.print(" Brazil NBOT_3: HIGH Pressure. NBOT is OFF");
    sms.endSMS();

    sms.beginSMS(textNum2);
    sms.print(" Brazil NBOT_3: HIGH Pressure. NBOT is OFF");
    sms.endSMS();
    
    sms.beginSMS(textNum3);
    sms.print(" Brazil NBOT_3: HIGH Pressure. NBOT is OFF");
    sms.endSMS();

    sms.beginSMS(textNum4);
    sms.print(" Brazil NBOT_3: HIGH Pressure. NBOT is OFF");
    sms.endSMS();
    
    sms.beginSMS(textNum5);
    sms.print(" Brazil NBOT_3: HIGH Pressure. NBOT is OFF");
    sms.endSMS();

    psi_max_flag = 0;

  } else if (temp_flag == 1) {
    sms.beginSMS(textNum1);
    sms.print(" Brazil NBOT_3: Temperature is high.");
    sms.endSMS();

    sms.beginSMS(textNum2);
    sms.print(" Brazil NBOT_3: Temperature is high.");
    sms.endSMS();
    
    sms.beginSMS(textNum3);
    sms.print(" Brazil NBOT_3: Temperature is high.");
    sms.endSMS();

    sms.beginSMS(textNum4);
    sms.print(" Brazil NBOT_3: Temperature is high.");
    sms.endSMS();
    
    sms.beginSMS(textNum5);
    sms.print(" Brazil NBOT_3: Temperature is high.");
    sms.endSMS();

    temp_flag = 0;
  }

  if (psi_min_flag) {
    sms.beginSMS(textNum1);
    sms.print(" Brazil NBOT_3: LOW Water pressure. NBOT is OFF");
    sms.endSMS();

    sms.beginSMS(textNum2);
    sms.print(" Brazil NBOT_3: LOW Water pressure");
    sms.endSMS();
    
    sms.beginSMS(textNum3);
    sms.print(" Brazil NBOT_3: LOW Water pressure");
    sms.endSMS();

    sms.beginSMS(textNum4);
    sms.print(" Brazil NBOT_3: LOW Water pressure");
    sms.endSMS();

    sms.beginSMS(textNum5);
    sms.print(" Brazil NBOT_3: LOW Water pressure");
    sms.endSMS();

    psi_min_flag = 0;
  }

  String lat;
  String lon;
  String acc;

  if (location.available()) {
    lat = String(location.latitude(), 7);
    lon = String(location.longitude(), 7);
    acc = String(location.accuracy());
  }

  String text_loc = " Brazil NBOT_3:  https://www.google.com/maps/place/" + lat + "," + lon + "   " + "Accuracy: +/- " + acc + "m";

  String serial_string = "Brazil NBOT_3    ";
  String temp_string = "Temperature:";
  temp_string.concat(motor_temp);
  String ozone_string = "  Ozone:";
  ozone_string.concat(ozoneConcentration);
  String pressure_string = "  Pressure:";
  pressure_string.concat(psi_voltage);
  
  String text_str = temp_string + ozone_string + pressure_string;

  if (sms_flag) {
    Serial.println("Message received from: ");
    sms.remoteNumber(senderNumber, 20);
    Serial.println(senderNumber);

    if (sms.peek() == 'G') {
      sms.flush();
      //String text_loc = "https://www.google.com/maps/place/" + lat + "," + lon + "   " + "Accuracy: +/- " + acc + "m";
      if (ON_OFF == 1) {
        text = "Brazil NBOT_3 Running" + text_str;
      } else {
        text = "Brazil NBOT_3 Running" + text_str;
      }
      delay(1000);
      sms.beginSMS(senderNumber);
      sms.print(text);
      //sms.print(text_loc);
      sms.endSMS();
    }
   
    sms.flush();
    Serial.println("message deleted");
  }
}
