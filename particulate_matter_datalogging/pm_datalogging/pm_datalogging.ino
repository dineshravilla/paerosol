/*
 * Author: Dinesh Ravilla
 * 
 * Temperature
 * Humidity
 * Particulate Matter - number concentration
 * Particulate Matter - mass concentration
 * Particulate Matter - typical particle size
 * Soil Moisture (used as humidity detector on surfaces - test)
 * Real Time Clock to timestamp data
 * 
 * Writes data to an excel sheet (.csv file), stored in SD card
 * Displays data on a touch screen 
 * 
 */

#include <sps30.h>
#include <SPI.h>
#include <SD.h>
#include <pm_temp.h>
#include "RTC.h"
#include "SparkFunHTU21D.h"
#include <LCDWIKI_GUI.h> //Core graphics library
#include <LCDWIKI_KBV.h> //Hardware-specific library

LCDWIKI_KBV pm_lcd(ILI9486,40,38,39,44,41); //model,cs,cd,wr,rd,reset

#define DHTPIN 15
#define DHTTYPE DHT11

//define some colour values
#define  BLACK   0x0000
#define BLUE    0x001F0000000000
#define RED     0xF800
#define GREEN   0x07E0
#define CYAN    0x07FF
#define MAGENTA 0xF81F
#define YELLOW  0xFFE0
#define WHITE   0xFFFF

DHT pm_temp(DHTPIN, DHTTYPE);
HTU21D pm_humid;
RTC_DS1307 pm_rtc;

char daysOfTheWeek[7][12] = {"Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"};

const int CS = 53;

const int AirValue = 620;   
const int WaterValue = 260;  
int intervals = (AirValue - WaterValue)/3;
int soilMoistureValue = 0;

void setup() {

  pm_lcd.Init_LCD();
  pm_lcd.Fill_Screen(BLACK);
  
  int16_t ret;
  uint8_t auto_clean_days = 4;
  uint32_t auto_clean;

  Serial.begin(9600);
  delay(2000);

  pinMode(53, OUTPUT);

  if(!pm_rtc.begin()) {
    Serial.println("Couldn't find RTC");
    while(1);
  }

  if(! pm_rtc.isrunning()) {
    Serial.println("RTC is not running.");
    //pm_rtc.adjust(DateTime(F(__DATE__), F(__TIME__)));
  }

  //pm_rtc.adjust(DateTime(now.unixtime());
  
  if(!SD.begin(CS)) {
    Serial.println("Card failed or not present.");
    return 0;
  } 

  Serial.println("Card initialized.");

  File test1 = SD.open("massconc.csv");
  if(test1) {
    SD.remove("massconc.csv");
  }

  File test2 = SD.open("numconc.csv");
  if(test2) {
    SD.remove("numconc.csv");
  }

  File test3 = SD.open("typsize.csv");
  if(test3) {
    SD.remove("typsize.csv");
  }

  File dataFile1 = SD.open("massconc.csv", FILE_WRITE);
  if(dataFile1) {
    dataFile1.println("Time, Temperature, Humidity, PM1, PM2.5, PM4, PM10");
    dataFile1.close();
    } else {
    Serial.println("Error opening mass_conc.csv");
  }
  
  delay(3000);
  
  File dataFile2 = SD.open("numconc.csv", FILE_WRITE);
  if(dataFile2) {
    dataFile2.println("Time, Temperature, Humidity, PM0.5, PM1, PM2.5, PM4, PM10");
    dataFile2.close();  
  } else {
    Serial.println("Error opening number_conc.csv"); 
  }

  delay(3000);

  File dataFile3 = SD.open("typsize.csv", FILE_WRITE);
  if(dataFile3) {
    dataFile3.println("Time, Temperature, Humidity, Typical Particle Size");
    dataFile3.close();  
  } else {
    Serial.println("Error opening number_conc.csv"); 
  }

  File dataFile4 = SD.open("moisture.csv", FILE_WRITE);
  if(dataFile4) {
    dataFile4.println("Time, Temperature, Humidity, Moisture, Level");
    dataFile4.close();
  } else {
    Serial.println("Error opening moisture.csv");
  }

  sensirion_i2c_init();

  while (sps30_probe() != 0) {
    Serial.print("SPS sensor probing failed\n");
    delay(500);
  }

  Serial.print("SPS sensor probing successful\n");

  ret = sps30_set_fan_auto_cleaning_interval_days(auto_clean_days);
  if (ret) {
    Serial.print("error setting the auto-clean interval: ");
    Serial.println(ret);
  }

  ret = sps30_start_measurement();
  if (ret < 0) {
    Serial.print("error starting measurement\n");
  }

  Serial.print("measurements started\n");

  pm_temp.begin();

  pm_humid.begin();
  
  delay(1000);
}


void loop() {

  DateTime now = pm_rtc.now();
  
  pm_lcd.Set_Text_Mode(0);
  pm_lcd.Fill_Screen(0x0000);
  pm_lcd.Set_Text_colour(RED);
  pm_lcd.Set_Text_Back_colour(BLACK);
  pm_lcd.Set_Text_Size(2.7);
  
  struct sps30_measurement m;
  char serial[SPS30_MAX_SERIAL_LEN];
  uint16_t data_ready;
  int16_t ret;

  //float humid = pm_temp.readHumidity();
  //float temp = pm_temp.readTemperature(true);

  float humid = pm_humid.readHumidity();
  float temp = pm_humid.readTemperature();

  soilMoistureValue = analogRead(A0);  //put Sensor insert into soil
  
  File dataFile1 = SD.open("massconc.csv", FILE_WRITE);
  File dataFile2 = SD.open("numconc.csv", FILE_WRITE);
  File dataFile3 = SD.open("typsize.csv", FILE_WRITE);
  File dataFile4 = SD.open("moisture.csv", FILE_WRITE);

  do {
    ret = sps30_read_data_ready(&data_ready);
    if (ret < 0) {
      Serial.print("error reading data-ready flag: ");
      Serial.println(ret);
    } else if (!data_ready)
      Serial.print("data not ready, no new measurement available\n");
    else
      break;
    delay(100); /* retry in 100ms */
  } while (1);

  ret = sps30_read_measurement(&m);
  if (ret < 0) {
    Serial.print("error reading measurement\n");
  } else {
      if(dataFile1) {
        dataFile1.print(now.year(), DEC);
        dataFile1.print('/');
        dataFile1.print(now.month(), DEC);
        dataFile1.print('/');
        dataFile1.print(now.day(), DEC);
        dataFile1.print(" (");
        dataFile1.print(daysOfTheWeek[now.dayOfTheWeek()]);
        dataFile1.print(") ");
        dataFile1.print(now.hour(), DEC);
        dataFile1.print(':');
        dataFile1.print(now.minute(), DEC);
        dataFile1.print(':');
        dataFile1.print(now.second(), DEC);
        dataFile1.print(", ");
               
        dataFile1.print(temp);
        dataFile1.print(",");
        dataFile1.print(humid);
        dataFile1.print(",");
        dataFile1.print(m.mc_1p0);
        dataFile1.print(",");
        dataFile1.print(m.mc_2p5);
        dataFile1.print(",");
        dataFile1.print(m.mc_4p0);
        dataFile1.print(",");
        dataFile1.print(m.mc_10p0);
        dataFile1.println();
        dataFile1.close();
      } else {
        Serial.println("Error opening massconc.csv");
      }

      if(dataFile2) {
        dataFile2.print(now.year(), DEC);
        dataFile2.print('/');
        dataFile2.print(now.month(), DEC);
        dataFile2.print('/');
        dataFile2.print(now.day(), DEC);
        dataFile2.print(" (");
        dataFile2.print(daysOfTheWeek[now.dayOfTheWeek()]);
        dataFile2.print(") ");
        dataFile2.print(now.hour(), DEC);
        pm_lcd.Print_Number_Int(now.hour(), 110, 360, 0, ' ', 10);
        dataFile2.print(':');
        pm_lcd.Print(":", 135, 360);
        dataFile2.print(now.minute(), DEC);
        pm_lcd.Print_Number_Int(now.minute(), 145, 360, 0, ' ', 10);
        dataFile2.print(':');
        pm_lcd.Print(":", 170, 360);
        dataFile2.print(now.second(), DEC);
        pm_lcd.Print_Number_Int(now.second(), 180, 360, 0, ' ', 10);
        dataFile2.print(", ");

        
        dataFile2.print(temp);
        dataFile2.print(",");
        pm_lcd.Print_String("Temperature: ", 10, 40);
        pm_lcd.Print_Number_Float(temp, 2, 180, 40, '.', 0, ' ');
        dataFile2.print(humid);
        dataFile2.print(",");
        pm_lcd.Print_String("Humidity: ", 10, 80);
        pm_lcd.Print_Number_Float(humid, 2, 180, 80, '.', 0, ' ');
        dataFile2.print(m.nc_0p5);
        dataFile2.print(",");
        pm_lcd.Print_String("PM 0.5: ", 10, 120);
        pm_lcd.Print_Number_Float(m.nc_0p5, 2, 180, 120, '.', 0, ' ');
        dataFile2.print(m.nc_1p0 - m.nc_0p5);
        dataFile2.print(",");
        pm_lcd.Print_String("PM 1.0: ", 10, 160);
        pm_lcd.Print_Number_Float(m.nc_1p0 - m.nc_0p5, 2, 180, 160, '.', 0, ' ');
        dataFile2.print(m.nc_2p5 - m.nc_1p0);
        dataFile2.print(",");
        pm_lcd.Print_String("PM 2.5: ", 10, 200);
        pm_lcd.Print_Number_Float(m.nc_2p5 - m.nc_1p0, 2, 180, 200, '.', 0, ' ');
        dataFile2.print(m.nc_4p0 - m.nc_2p5);
        dataFile2.print(",");
        pm_lcd.Print_String("PM 4.0: ", 10, 240);
        pm_lcd.Print_Number_Float(m.nc_4p0 - m.nc_2p5, 2, 180, 240, '.', 0, ' ');
        dataFile2.print(m.nc_10p0 - m.nc_4p0);
        dataFile2.println();
        pm_lcd.Print_String("PM 10.0: ", 10, 280);
        pm_lcd.Print_Number_Float(m.nc_10p0 - m.nc_4p0, 2, 180, 280, '.', 0, ' ');
        dataFile2.close();
      } else {
        Serial.println("Error opening numconc.csv");
      }

      if(dataFile3) {
        dataFile3.print(now.year(), DEC);
        dataFile3.print('/');
        dataFile3.print(now.month(), DEC);
        dataFile3.print('/');
        dataFile3.print(now.day(), DEC);
        dataFile3.print(" (");
        dataFile3.print(daysOfTheWeek[now.dayOfTheWeek()]);
        dataFile3.print(") ");
        dataFile3.print(now.hour(), DEC);
        dataFile3.print(':');
        dataFile3.print(now.minute(), DEC);
        dataFile3.print(':');
        dataFile3.print(now.second(), DEC);
        dataFile3.print(", ");
        
        dataFile3.print(temp);
        dataFile3.print(",");
        dataFile3.print(humid);
        dataFile3.print(",");
        dataFile3.print(m.typical_particle_size);
        dataFile3.println();
        dataFile3.close();
      } else {
        Serial.println("Error opening typsize.csv");
      }

      if(dataFile4) {
        dataFile4.print(now.year(), DEC);
        dataFile4.print('/');
        dataFile4.print(now.month(), DEC);
        dataFile4.print('/');
        dataFile4.print(now.day(), DEC);
        dataFile4.print(" (");
        dataFile4.print(daysOfTheWeek[now.dayOfTheWeek()]);
        dataFile4.print(") ");
        dataFile4.print(now.hour(), DEC);
        dataFile4.print(':');
        dataFile4.print(now.minute(), DEC);
        dataFile4.print(':');
        dataFile4.print(now.second(), DEC);
        dataFile4.print(", ");

        dataFile4.print(temp);
        dataFile4.print(",");
        dataFile4.print(humid);
        dataFile4.print(",");
        dataFile4.print(soilMoistureValue);
        //pm_lcd.Print_String("Moisture: ", 10, 320);
        //pm_lcd.Print_Number_Int(soilMoistureValue, 180, 320, 0, ' ', 10);

        if(soilMoistureValue > WaterValue && soilMoistureValue < (WaterValue + intervals)) {
          dataFile4.println("Very Wet");
          //pm_lcd.Print_String("Very Wet", 180, 350);
        } else if(soilMoistureValue > (WaterValue + intervals) && soilMoistureValue < (AirValue - intervals)) {
          dataFile4.println("Wet");
          //pm_lcd.Print_String("Wet", 180, 350);
        } else if(soilMoistureValue < AirValue && soilMoistureValue > (AirValue - intervals)) {
          dataFile4.println("Dry");
          //pm_lcd.Print_String("Dry", 180, 350);
        }
        dataFile4.println();
        dataFile4.close();
        } else {
          Serial.println("Error opening moisture.csv");
        }
      
      Serial.println();
    }

  delay(60000);
}
